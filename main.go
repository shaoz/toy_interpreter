package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
	"unicode"
	"unicode/utf8"
)

const (
	// INTEGER type
	INTEGER = "INT"
	// PLUS type
	PLUS = "ADD"
	// MINUS type
	MINUS = "SUB"
	// MULTIPLY type
	MULTIPLY = "MUL"
	// DIVIDE type
	DIVIDE = "DIV"
	// EOF type
	EOF = "EOF"
	// EOFRUNE char
	EOFRUNE = rune('\x00')
)

// OPERATIONS that are valid
var OPERATIONS = map[string]string{
	PLUS:     "+",
	MINUS:    "-",
	MULTIPLY: "*",
	DIVIDE:   "/",
}

func main() {
	var intptr interpreter
	var lexr lexer
	for {
		reader := bufio.NewReader(os.Stdin)
		fmt.Print("calc> ")
		text, _ := reader.ReadString('\r')
		text = text[:len(text)-1]
		if len(text) == 0 {
			continue
		}
		lexr.init(text)
		intptr.init(&lexr)
		fmt.Println(intptr.expr())
	}

	// lexr.init("1 - 2")
	// intptr.init(&lexr)
	// fmt.Println(intptr.expr())
}

type langToken struct {
	tokenType, strValue string
	intValue            int
}

func (t langToken) String() string {
	var str strings.Builder
	fmt.Fprintf(&str, "Token(%s, %s, %d)", t.tokenType, t.strValue, t.intValue)
	return str.String()
}

/*
 * lexer code
 */
type lexer struct {
	text     string
	pos      int
	currRune rune
}

// set a new lexer
func (lexr *lexer) init(text string) {
	lexr.text = text
	lexr.pos = 0
	lexr.currRune, _ = utf8.DecodeRuneInString(lexr.text[0:])
}

func (lexr *lexer) error(formatStr string, err ...interface{}) {
	fmt.Println("Lexer error")
	fmt.Printf(formatStr, err...)
	fmt.Print("\n")
}

func (lexr *lexer) advance() {
	lexr.pos++
	if lexr.pos > len(lexr.text)-1 {
		lexr.currRune = EOFRUNE
	} else {
		lexr.currRune, _ = utf8.DecodeRuneInString(lexr.text[lexr.pos:])
	}
}

func (lexr *lexer) skipWhitespace() {
	for lexr.currRune != EOFRUNE && unicode.IsSpace(lexr.currRune) {
		lexr.advance()
	}
}

func (lexr *lexer) integer() int {
	digits := []rune{}
	for lexr.currRune != EOFRUNE && unicode.IsDigit(lexr.currRune) {
		digits = append(digits, lexr.currRune)
		lexr.advance()
	}
	number := string(digits)
	if val, err := strconv.Atoi(number); err == nil {
		return val
	}

	lexr.error("Error parsing '%s'", number)
	return 0
}

func (lexr *lexer) getNextToken() (*langToken, bool) {
	for lexr.currRune != EOFRUNE {
		if unicode.IsSpace(lexr.currRune) {
			lexr.skipWhitespace()
			continue
		}

		if unicode.IsDigit(lexr.currRune) {
			return &langToken{INTEGER, "", lexr.integer()}, true
		}

		switch lexr.currRune {
		case '+':
			lexr.advance()
			return &langToken{PLUS, "+", 0}, true
		case '-':
			lexr.advance()
			return &langToken{MINUS, "-", 0}, true
		case '*':
			lexr.advance()
			return &langToken{MULTIPLY, "*", 0}, true
		case '/':
			lexr.advance()
			return &langToken{DIVIDE, "/", 0}, true
		}
		lexr.error("Error parsing rune '%c'", lexr.currRune)
		return nil, false
	}

	return &langToken{EOF, "", 0}, true
}

/*
 * Parser
 */
type interpreter struct {
	lexr      *lexer
	currToken *langToken
}

// set a new interpreter
func (intptr *interpreter) init(lexr *lexer) {
	intptr.lexr = lexr
	token, success := intptr.lexr.getNextToken()
	if !success {
		intptr.currToken = &langToken{EOF, "", 0}
	}
	intptr.currToken = token
}

func (intptr *interpreter) error(formatStr string, err ...interface{}) {
	fmt.Println("Syntax error")
	fmt.Printf(formatStr, err...)
	fmt.Print("\n")
}

func (intptr *interpreter) eat(tokenType string) bool {
	if intptr.currToken.tokenType == tokenType {
		nextToken, success := intptr.lexr.getNextToken()
		intptr.currToken = nextToken
		return success
	}

	intptr.error("Error eating '%s'", intptr.currToken)
	return false
}

func (intptr *interpreter) factor() int {
	token := intptr.currToken
	factorSuccess := intptr.eat(INTEGER)
	if factorSuccess {
		return token.intValue
	}
	return 0
}

func (intptr *interpreter) expr() string {
	result := intptr.factor()

	for _, ok := OPERATIONS[intptr.currToken.tokenType]; ok; _, ok = OPERATIONS[intptr.currToken.tokenType] {
		op := intptr.currToken
		opSuccess := intptr.eat(op.tokenType)

		if opSuccess {
			switch op.tokenType {
			case PLUS:
				result += intptr.factor()
			case MINUS:
				result -= intptr.factor()
			case MULTIPLY:
				result *= intptr.factor()
			case DIVIDE:
				result /= intptr.factor()
			}
		}
	}

	eofSuccess := intptr.eat(EOF)

	if eofSuccess {
		return strconv.Itoa(result)
	}

	return ""
}
