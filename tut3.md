# Write an interpreter 3

* to handle arbirtrary numbers of operations, we need to define a recursive expression syntax
    * an expr is:
        * term
        * term op expr
    * a term is an int for now

* syntax diagrams help represent a grammar and also help in implementation

* parsing is also called syntax analysis

* full interpreter has 3 parts
    1. lexer - takes input and converts into stream of tokens

    2. parser - takes stream of tokens and tries to recognize a structure

    3. interpreter - generates results after parser has found a valid expression structure

# questions
1. What is a syntax diagram?
    - diagram that represents a grammar that we can translate almost directly into an implementation

2. What is syntax analysis?
    - taking a stream of tokens and trying to recognize a structure

3. What is a syntax analyzer?
    - something that does syntax analysis
