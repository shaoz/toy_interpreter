# Write an interpreter 2

* to handle multidigit numbers, we need to decouple tokens from being just one digit
* to handle whitespace characters, we need to have a skipWhitespace function
* to add operations, we just need to add a new token type and add the logic for it

* handle multidigit tokens
    * instead of having just a pointer to a position in the source, we want a current character value as well in the interpreter
    * when we reach the end, we can just set this value to the null character so that we know it's the end of the source

    * advance
        * sets pos and current character in the interpreter, basically like getNextToken but getNextChar

    * integer
        * have a running string we add to while current character is a digit
        * return the final int value

    * getNextToken
        * now iterates over the currentChars as we advance through the source

* handle whitespace

    * skipWhitespace
        * while we're not at the end of the source and while the current character is whitespace, we just advance the current character and pointer to position in source

* lexeme
    * a sequence of chars that forms a token

* parsing
    * what expr does, basically
    * making sense of the stream of tokens that we are getting from getNextToken, by settings rules for valid expressions/statements

# questions
1. What is a lexeme?
    - characters that make up a logical token

2. What is the name of the process that finds the structure in the stream of tokens, or put differently, what is the name of the process that recognizes a certain phrase in that stream of tokens?
    - parsing

3. What is the name of the part of the interpreter (compiler) that does parsing?
    - parser
