# Write an interpreter 1

* first we need a representation for tokens, minimally:
    1. number literals
    2. operations

* then, interpreter representation, to hold:
    1. source
    2. pointer to current char
    3. current token

* interpreter should have a getNextToken, eat, and expr

    * getNextToken
        * lexical analyzer/tokenizer/scanner
        * generates token stream, one token at a time from characters
        * if unexpected character, error

    * eat
        * checks if the current token we are looking at matches the token type we are expecting
        * if yes, consume the token by advancing the pointer and getting the next token
        * if not, error

    * expr
        * evals expressions and defines rules for what is a valid expression

# questions

1. What is an interpreter?
    - executes code directly from the source

2. What is a compiler?
    - changes source code to another form, maybe assembly or some other language, before executing

3. What’s the difference between an interpreter and a compiler?
    - compilation step

4. What is a token?
    - logical unit in a language, op or number for now

5. What is the name of the process that breaks input apart into tokens?
    - lexing/scanning/tokenizing

6. What is the part of the interpreter that does lexical analysis called?
    - lexer/scanner/tokenizer

7. What are the other common names for that part of an interpreter or a compiler?
    - see above
